(use-modules
 (ice-9 match)
 (goblins)
 (goblins actor-lib cell)
 (goblins actor-lib common)
 (goblins actor-lib methods)
 ;;(srfi srfi-1)
 (web request)
 (web response)
 (web server)
 (web uri))

;; TODO: Change this! The Guile docs say this isn't suitable for
;; security-critical uses.
(set! *random-state* (random-state-from-platform))
(define dbg-hostname "bgateway.terracrypt.net")

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define (not-found request)
  (list (build-response #:code 404)
        (string-append "Resource not found: "
                       (uri->string (request-uri request)))))
(define (unauthorized request)
  (list (build-response #:code 403)
        (string-append "Access denied: "
                       (uri->string (request-uri request)))))

(define (^dispatcher bcom)
  (define proxies (spawn ^ghash))
  (methods
   [(handle request body)
    (match (request-path-components request)
      (("obj" id)
       (cond
        [($ proxies 'has-key? id)
         ($ ($ proxies 'ref id) 'handle request body)]
        [else
         (not-found request)])))]
   [(add id targ)
    ($ proxies 'set id targ)]))

(define (start-server)
  (define dispatch (main-vat
                    (lambda ()
                      (spawn ^dispatcher))))
  ;;(define dispatch (main-run (spawn ^dispatcher))) ;; TODO: Fails for some reason!
  (define (handle-dispatch request body)
    (apply values
           (main-vat
            (lambda ()
              ($ dispatch 'handle request body)))))
  (run-server handle-dispatch))

(define (random-string n)
  "Generate a random lowercase ASCII string"
  (list->string
   (map integer->char
        (map
         ;; Random lowercase ASCII character
         (lambda (x)
           (+ 97 (random 26)))
         ;; n times, in a list
         ;; (Is there a better way to do this? Could just be [0] * n in python)
         (iota n)))))

(define (^bearcap-proxy bcom targ)
  (define objid (random-string 30))
  (define bear (random-string 30))
  (define uri (build-uri 'bear
                         #:query (format #f "t=~a" bear)
                         #:host dbg-hostname ;; TODO: support config
                         #:path (format #f "/obj/~a" objid)))
  (methods
   [(get-bearcap)
    (uri->string uri)]
   [(handle request request-body)
    ($ targ)])) ;; TODO: actual implementation here

;; https://stackoverflow.com/a/5700232
;; We'll need something like this later
(define (deep-map f l)
  (let deep ((x l))
    (cond ((null? x) x)
          ((pair? x) (map deep x))
          (else (f x)))))

;; TEST CODE
(define test-req (build-request (build-uri 'https #:host "terracrypt.net" #:path "/obj/test")))

(define (^hello bcom)
  (lambda ()
    "Hello world!"))

(define main-vat (spawn-vat))
(define-vat-run main-run main-vat)
(define hello (main-run (spawn ^hello)))
